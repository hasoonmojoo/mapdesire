using System.Collections;
using UnityEngine;

public class Blood : MonoBehaviour
{
    public float bloodTime = 0.2f;

    private void OnEnable()
    {
        StartCoroutine("AutoDisable");
    }


    private IEnumerator AutoDisable()
    {
        // 0.1초 후에 오브젝트가 사라지도록 한다
        yield return new WaitForSeconds(bloodTime);

        gameObject.SetActive(false);
    }
}
