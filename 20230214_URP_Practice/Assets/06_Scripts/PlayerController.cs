using Cinemachine;
using Unity.Mathematics;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float moveSpeed;
    [SerializeField] private float jumpForce = 5.0f;        // 점프 세기
    [SerializeField] private float rotationSpeed = 5.0f;    // 회전 속도
    [SerializeField] private float runSpeed = 10.0f;        // 달리기 속도
    [SerializeField] private float walkSpeed = 5.0f;        // 이동 속도
    [SerializeField] GameObject attackCollision;            // 공격 범위 콜라이더
    [SerializeField] int hp = 100;                          // 체력

    private Rigidbody rb;
    private Animator animator;
    private Camera mainCamera;
    private Transform cameraTransform;
    private Vector3 cameraOffset;

    [SerializeField] bool isGrounded = false;
    [SerializeField] bool isFall = false;

    
    public LayerMask groundLayer;   // 그라운드 체크 레이어
    public float rayRange = 0.3f;   // 그라운드 체크 거리

    CameraController camCon;

    public CinemachineVirtualCamera enemyFollowCam;
    
    
    
    // 공격 관련==================================================================================
    public float attackCooltime = 0.5f;   // 공격 쿨타임
    float currentTime;
    /*bool attackOver = true;             // 공격 애니메이션의 마지막 부분에 달아서 공격이 끝났을 때에만
                                        // currentTime에 Time.deltaTime을 더하도록 할 예정*/
    // 혹은 기력으로 공격 회피 조절

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        mainCamera = Camera.main;
        cameraTransform = mainCamera.transform;
        cameraOffset = transform.position - cameraTransform.position;
        camCon = FindObjectOfType<CameraController>();
    }

    private void FixedUpdate()
    {
        Move();
        Falling();
        FallingCancel();

        Debug.Log(rb.velocity.y);
        Debug.Log(isGrounded);
    }

    private void Update()
    {
        GroundCheck();
        Jump();
        Attack();
        Rolling();
    }

    void Move()
    {
        if (!isGrounded || isFall)
        {
            return;
        }
        
        currentTime += Time.deltaTime;

        // Horizontal movement
        float xInput = Input.GetAxisRaw("Horizontal");
        float zInput = Input.GetAxisRaw("Vertical");

        moveSpeed = walkSpeed;

        if (zInput != 0 || xInput != 0)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                animator.SetBool("isRun", true);
                animator.SetBool("isWalk", false);
                moveSpeed = runSpeed;

            }
            else
            {
                animator.SetBool("isWalk", true);
                animator.SetBool("isRun", false);
            }
        }
        else
        {
            animator.SetBool("isWalk", false);
            animator.SetBool("isRun", false);
        }

        Vector3 moveDirection = new Vector3(xInput, 0.0f, zInput);




        // Turning
        if (moveDirection != Vector3.zero && !camCon.isFixingDirection)
        {
            moveDirection = cameraTransform.TransformDirection(moveDirection);
            moveDirection.y = 0.0f;
            moveDirection.Normalize();
            rb.velocity = moveDirection * moveSpeed;

            Quaternion targetRotation = Quaternion.LookRotation(moveDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        }

        else if(enemyFollowCam.enabled && camCon.isFixingDirection)
        {
            moveDirection = enemyFollowCam.transform.TransformDirection(moveDirection);
            moveDirection.y = 0.0f;
            moveDirection.Normalize();
            rb.velocity = moveDirection * moveSpeed;

            transform.forward = enemyFollowCam.transform.forward;

            /*Quaternion targetRotation = Quaternion.LookRotation(moveDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);*/
        }
    }

    void Falling()
    {
        if(isGrounded)
            return;
        rb.AddForce(Vector3.down * 15f, ForceMode.Acceleration);
        if (rb.velocity.y <= -10)
        {
            isFall = true;
            animator.SetBool(("isFall"), isFall);
        }
    }

    void FallingCancel()
    {
        if (isFall)
        {
            if(Physics.Raycast(transform.position, Vector3.down, 30))
            {
                isFall = false;
                animator.SetBool(("isFall"), isFall);
            }
        }
    }
    
    void Rolling()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            animator.SetTrigger("Rolling");
        }
    }

    void Attack()
    {
        if (Input.GetMouseButtonDown(0) && currentTime >= attackCooltime)
        {
            currentTime = 0;
            rb.velocity = Vector3.zero;
            animator.SetTrigger("Attack");
        }
    }

    void Jump()
    {
        // Jumping
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
            animator.SetTrigger("Jump");
        }
    }

    // 어택 콜라이더 On <- 애니메이션 event 에서 호출
    public void OnAttackCollision()
    {
        Debug.Log("공격 콜라이더 생성");
        attackCollision.SetActive(true);

    }

    void GroundCheck()
    {
        // 어떤 콜라이더에서 떼어진 경우 isGrounded를 false로 변경
        if (Physics.Raycast(transform.position, Vector3.down, rayRange, groundLayer))
        {
            isGrounded = true;
            animator.SetBool("isGrounded", isGrounded);
        }
        else
        {
            isGrounded = false;
            animator.SetBool("isGrounded", isGrounded);
        }

        Debug.DrawRay(transform.position, Vector3.down, Color.red, rayRange);
    }

    public void TakeDamage(int damage)
    {
        hp -= damage;
        // 체력이 감소되거나 피격 애니메이션이 재생되는 등의 코드를 작성
        Debug.Log(damage + " 만큼 플레이어의 체력이 감소합니다.");
        // 피격 애니메이션 재생
        //animator.SetTrigger("onHit");
        // 색상 변경
        //StartCoroutine("OnHitColor");
        // hp가 0 이하일 때 Die 메서드 호출
        if (hp <= 0)
        {
            hp = 0;
           //StartCoroutine("BossDie");
        }
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            animator.SetBool("isGrounded", true);
        }

        // 어떤 콜라이더와 닿았으며, 충돌 표면이 위쪽을 보고 있으면
        // 0.7 첫 번째(contacts[0]) 충돌 지점의 표면 방향이 '위쪽이며', '경사가 45도 이하인지' 검사
        // 1에 가까울 수록 경사는 완만해 진다.
        /*if (collision.contacts[0].normal.y > 0.7f)
        {
            // isGrounded를 true로 변경하고, 누적 점프 횟수를 0으로 리셋
            isGrounded = true;
        }#1#
    }

    private void OnCollisionExit(Collision collision)
    {
        // 어떤 콜라이더에서 떼어진 경우 isGrounded를 false로 변경
        if (Physics.Raycast(transform.position, Vector3.down, rayRange, groundLayer))
        {
            isGrounded = true;
            animator.SetBool("isGrounded", isGrounded);
        }
        else
        {
            isGrounded = false;
            animator.SetBool("isGrounded", isGrounded);
        }

        Debug.DrawRay(transform.position, Vector3.down, Color.red, rayRange);
    }*/

    private void LateUpdate()
    {
        //cameraTransform.position = transform.position - cameraOffset;
    }
}
