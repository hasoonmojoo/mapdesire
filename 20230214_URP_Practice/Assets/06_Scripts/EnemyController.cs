//using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    private Animator animator;              // 애니메이터
    private SkinnedMeshRenderer skinRenderer;  // 피격용 렌더러
    private Color originColor;              // 원래 색(피격시 붉게 변했다가 원래 색으로 돌아옴)
    private int hp;                         // 보스 체력
    public int maxHp;                       // 보스 최대 체력

    public Image white;                     // 보스 사망 시 페이드 아웃 이미지
    Color color;                            // 페이드 아웃 알파 값 수정용
    float waitTime = 0.15f;                 // 페이드 아웃 시간

    public float attackCooltime = 2f;       // 보스 공격 쿨타임
    private float currentTime;              // 보스 쿨타임 측정 시간

    public GameObject player;               // 타겟 플레이어
    public float meleeAttackDistance;       // 근접 공격 가능 거리
    public GameObject attackCollision;      // 근접 공격 콜라이더
    public GameObject rangeAttackCollision;  // 원거리 공격 콜라이더

    /*private Vector2 originPos;              // 원래 자리로 돌아갈 자리 저장

    public GameObject[] summonPos;          // 소환수 자리
    public GameObject summon;               // 소환수*/

    //public GameObject stageClearText;       // 스테이지 클리어 텍스트
    private bool isDie = false;             // 보스 생존 여부
    //public GameObject hurtEffect;           // 보스 피격 이펙트
    public Material red;
    public Material[] origins;

    NavMeshAgent navMeshAgent;
    Rigidbody rigidbody;

    //private Material[] copyMat;

    //public GameObject titleAndExitBtn;      // 보스 클리어 후 등장할 버튼

    // Start is called before the first frame update
    void Start()
    {
        //originPos = transform.position;             // 순간 이동 후 다시 돌아갈 원래 자리
        //player = GameObject.FindWithTag("Player");  // 플레이어 찾기
        hp = maxHp;                                 // 보스 체력
        animator = GetComponent<Animator>();
        skinRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        originColor = skinRenderer.material.color;         // 피격 시 붉은 상태 깜빡임
        //color = white.color;                        // 피격 시 화이트 페이드 인 아웃

        //titleAndExitBtn.SetActive(false);

        navMeshAgent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
    }

    /*void ChangeMat()
    {
        for (int i = 0; i < origins.Length; i++)
        {
            copyMat[i] = origins[i];
            origins[i] = red;
        }
    }

    void originMat()
    {
        for (int i = 0; i < origins.Length; i++)
        {
            origins[i] = copyMat[i];
        }
    }*/
   
    // 스킨메쉬렌더러의 색깔을 바꿔 피격 이펙트
    private void SetColor(Material mats, int num)
    {
        foreach(Transform tf_Child in this.transform)
        {
            if(tf_Child.name == "pelvis fabic.005")
            {
                var newmaterials = new Material[tf_Child.GetComponent<Renderer>().materials.Length];

                for (int i = 0; i < newmaterials.Length; i++)
                {
                    if (num == 1)
                    {
                        newmaterials[i] = mats;
                    }
                    else
                    {
                        newmaterials[i] = origins[i];
                    }
                }

                tf_Child.GetComponent<Renderer>().materials = newmaterials;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isDie)
            return;
        currentTime += Time.deltaTime;
        if (currentTime >= attackCooltime)
        {
            currentTime = 0;
            AttackAI();
        }
    }

    bool ClosePlayer()
    {
        // 플레이어와의 거리가 가깝다면
        float distance = Vector2.Distance(player.transform.position, transform.position);
        if (Mathf.Abs(distance) <= meleeAttackDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void AttackAI()
    {
        if (ClosePlayer())
        {
            if (hp / maxHp > 0.7f)
                meleeAttack();
            else if (hp / maxHp > 0.3f)
                ComboAttack();

        }
        else
        {
            if (hp / maxHp > 0.5f)
                Chasing();
            else
                RangeAttack();
        }
    }
    //===============================================
    // 애니메이션 호출 메서드 모음
    public void meleeAttack()
    {
        animator.SetBool("onChase", false);
        navMeshAgent.isStopped = true;
        int aktNum = Random.Range(1, 4);
        animator.SetInteger("atkNum", aktNum);
    }

    public void ComboAttack()
    {
        animator.SetBool("onChase", false);
        navMeshAgent.isStopped = true;
        int aktNum = Random.Range(1, 4);
        animator.SetInteger("comNum", aktNum);
    }

    public void RangeAttack()
    {
        animator.SetTrigger("jumpAtk");
    }

    public void Chasing()
    {
        animator.SetBool("onChase", true);
        navMeshAgent.SetDestination(player.transform.position);
        navMeshAgent.stoppingDistance = 5f;
    }

    //========================================================================
    // 애니메이션 기능 메서드
    public void BasicAttackCollision()
    {
        attackCollision.SetActive(true);
    }

    public void RangeAttackCollision()
    {
        rangeAttackCollision.SetActive(true);
    }

    public void JumpTeleport()
    {
        rigidbody.MovePosition(new Vector3(player.transform.position.x, player.transform.position.y + 20f, player.transform.position.z));
    }

    // 스킬 시전 후 순간 이동 뒤에 기본 공격 후 원래 자리로 돌아감
    /*public void Teleport()
    {
        transform.position = new Vector3(player.transform.position.x + 2f, transform.position.y, transform.position.z);
        Invoke("OriginPosTele", 2f);
    }*/

    /*// 원래 자리로 돌아감 -> 추후 플레이어가 탐색 범위를 벗어났을 때 사용 예정
    void OriginPosTele()
    {
        transform.position = originPos;
    }*/

    /*public void Summon()
    {
        int rand = Random.Range(0, 2);
        Instantiate(summon, summonPos[rand].transform.localPosition, transform.rotation);
        Debug.Log("소환수 소환");
    }*/

    public void TakeDamage(int damage)
    {
        hp -= damage;
        // 체력이 감소되거나 피격 애니메이션이 재생되는 등의 코드를 작성
        Debug.Log(damage + "의 체력이 감소합니다.");
        // 피격 애니메이션 재생
        animator.SetTrigger("onHit");
        // 색상 변경
        StartCoroutine("OnHitColor");
        // hp가 0 이하일 때 Die 메서드 호출
        if (hp <= 0)
        {
            hp = 0;
            StartCoroutine("BossDie");
        }
    }

    IEnumerator BossDie()
    {
        isDie = true;
        StartCoroutine("FadeIn");                 // 페이드 아웃
        yield return new WaitForSeconds(2f);

        animator.SetTrigger("isDie");
        StartCoroutine("FadeOut");                // 페이드 아웃
        //SoundManager.instance.PlaySoundEffect("BossDie");   // 보스 죽음 효과음
        yield return new WaitForSeconds(2f);

        //SoundManager.instance.PlaySoundEffect("Clear");     // 클리어 효과음
        /*stageClearText.gameObject.SetActive(true);          // 클리어 텍스트
        titleAndExitBtn.SetActive(true);*/

        color.a = 0f;
        white.color = color;
        //Destroy(gameObject);
        // 스테이지 클리어
    }

    // 페이드 인
    IEnumerator FadeIn()
    {
        while (white.color.a <= 1f)
        {
            color.a += 0.1f;
            white.color = color;
            yield return new WaitForSeconds(waitTime);
        }
        color.a = 1f;
        white.color = color;
    }

    // 페이드 아웃
    IEnumerator FadeOut()
    {
        while (white.color.a > 0)
        {
            color.a -= 0.1f;
            white.color = color;
            yield return new WaitForSeconds(waitTime);
        }
        color.a = 0f;
        white.color = color;
    }


    private IEnumerator OnHitColor()
    {
        // 색을 빨간색으로 변경한 후 0.1초 후에 원래 색상으로 변경
        SetColor(red, 1);
        // 피격 이펙트 활성화
        //hurtEffect.SetActive(true);
        // 카메라 쉐이크
        //Camera.main.DOShakePosition(0.1f, 0.3f);
        // 느려짐 연출
        //Time.timeScale = 0.1f;
        // 피격 효과음 랜덤 호출
        int random = Random.Range(1, 4);
        //SoundManager.instance.PlaySoundEffect("Sword" + random);
        // 대기 시간
        yield return new WaitForSeconds(0.2f);
        // 피격 이펙트 비활성화
        //hurtEffect.SetActive(false);
        // 느려짐 정상화
        //Time.timeScale = 1;
        // 피격 색깔 정상화
        SetColor(red, 2);
    }
}
