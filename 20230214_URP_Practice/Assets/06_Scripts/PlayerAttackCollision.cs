using System.Collections;
using UnityEngine;

public class PlayerAttackCollision : MonoBehaviour
{
    public float collisionTime = 0.1f;
    public int atk = 10;

    public GameObject blood;            // 타격 이펙트

    private void OnEnable()
    {
        StartCoroutine("AutoDisable");
    }

    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("바호메트");

        // 플레이어가 타격하는 대상의 태그, 컴포넌트, 함수는 바뀔 수 있다
        if (collision.CompareTag("Boss"))
        {
            collision.GetComponent<EnemyController>().TakeDamage(atk);
            blood.SetActive(true);
        }
    }

    private IEnumerator AutoDisable()
    {
        // 0.1초 후에 오브젝트가 사라지도록 한다
        yield return new WaitForSeconds(collisionTime);

        gameObject.SetActive(false);
    }
}
