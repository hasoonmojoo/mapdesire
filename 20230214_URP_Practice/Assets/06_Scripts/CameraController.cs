using Cinemachine;
using System.Collections;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class CameraController : MonoBehaviour 
{
    [SerializeField] private Transform target;          // 카메라가 추적하는 대상
    [SerializeField] private float minDistance = 3;     // 카메라와 target의 최소 거리
    [SerializeField] private float maxDistance = 30;    // 카메라와 target의 최대 거리
    [SerializeField] private float wheelSpeed = 500;    // 마우스 휠 스크롤 속도
    [SerializeField] private float xMoveSpeed = 500;    // 카메라의 y축 회전 속도
    [SerializeField] private float yMoveSpeed = 250;    // 카메라의 x축 회전 속도

    [SerializeField] private float yMinLimit = 5;       // 카메라 x축 회전 제한 최소 값
    [SerializeField] private float yMaxLimit = 80;      // 카메라 x축 회전 제한 최대 값
    private float x, y;                                 // 마우스 이동 방향 값
    private float distance;                             // 카메라와 target의 거리
    private bool _istargetNull;
    //========================================================================================

    public bool isFixingDirection; // A flag to keep track of whether the camera direction is fixed to the enemy
    [SerializeField] private float detectionRange = 10f; // The range at which enemies can be detected
    [SerializeField] private LayerMask enemyLayer; // The layer that enemies belong to

    private Transform currentEnemy; // The enemy that is currently being tracked by the camera
    public Transform player;        // 플레이어 위치

    public CinemachineVirtualCamera enemyFollow;    // 적 타겟 카메라
    public CinemachineVirtualCamera playerPos;      // 플레이어 FollowCam위치용 버추얼 카메라
    public Camera playerCam;                        // 플레이어 FollowCam

    public GameObject lockonUI;                     // 락온 UI

    float currentYOffset;

    private void Start()
    {
        _istargetNull = player == null;
    }

    private void Awake()
    { 
        // 최초 설정된 target과 카메라의 위치를 바탕으로 distance 값 초기화
        distance = Vector3.Distance(transform.position, player.position);
        // 최초 카메라의 회전 값을 x, y 변수에 저장
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }

    private void Update()
    {
        if(_istargetNull) return;
        
        // 오른쪽 마우스 클릭 시
        if(Input.GetMouseButton(1))
        {
            // 마우스 x, y축 움직임 방향 정보
            x += Input.GetAxis("Mouse X") * xMoveSpeed * Time.deltaTime;
            y -= Input.GetAxis("Mouse Y") * yMoveSpeed * Time.deltaTime;
            // 오브젝트의 위/아래(x축) 한계 범위 설정
            y = ClampAngle(y, yMinLimit, yMaxLimit);
            // 카메라의 회전(Rotation) 정보 갱신
            if(!isFixingDirection)
                transform.rotation = Quaternion.Euler(y,x,0);
        }

        ZoomInZoomOut();

        DetectEnemy();
    }



    private void LateUpdate()
    {
        if(_istargetNull) return;
        
        // 카메라의 위치(Position) 정보 갱신
        // target의 위치를 기준으로 distance만큼 떨어져서 쫓아간다
        transform.position = transform.rotation * new Vector3(0, 5, -distance) + player.position;

    }

    float ClampAngle(float angle,float min,float max)
    {
        if (angle < -360) angle += 360;
        if (angle > 360) angle -= 360;

        return Mathf.Clamp(angle, min, max);
    }

    void ZoomInZoomOut()
    {
        // 마우스 휠 스크롤을 이용해 target과 카메라의 거리 값(distance) 조절
        distance -= Input.GetAxis("Mouse ScrollWheel") * wheelSpeed * Time.deltaTime;
        // 거리는 최소, 최대 거리를 설정해서 그 값을 벗어나지 않도록 한다
        distance = Mathf.Clamp(distance, minDistance, maxDistance);
    }

    public void LockOn()
    {
        Vector3 pos = currentEnemy.position + new Vector3(0, currentYOffset, 0);
        lockonUI.transform.position = pos;
        lockonUI.transform.localScale = Vector3.one * ((enemyFollow.transform.position - pos).magnitude * 0.1f);

        lockonUI.SetActive(true);
        playerCam.enabled = false;
        isFixingDirection = true;
        target = currentEnemy;
        enemyFollow.Priority = 15;
        enemyFollow.LookAt = currentEnemy;
        Debug.Log("보스 록온 ");
    }

    public void LockOff()
    {
        StopAllCoroutines();
        StartCoroutine("CameraSmoothMove");
    }

    IEnumerator CameraSmoothMove()
    {
        lockonUI.SetActive(false);

        currentEnemy = null;
        isFixingDirection = false;
        enemyFollow.LookAt = null;
        enemyFollow.Priority = 0;
        playerPos.Priority = 10;
        yield return new WaitForSeconds(1f);
        playerCam.enabled = true;
        playerPos.Priority = 0;
    }

    void DetectEnemy()
    {
        // Detect the closest enemy within the detection range
        Collider[] colliders = Physics.OverlapSphere(player.position, detectionRange, enemyLayer);
        float closestDistance = Mathf.Infinity;

        foreach (Collider collider in colliders)
        {
            float distance = Vector3.Distance(player.position, collider.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                currentEnemy = collider.transform;
                Debug.Log("적 감지");

                float h1 = currentEnemy.GetComponent<CapsuleCollider>().height;
                float h2 = currentEnemy.localScale.y;
                float h = h1 * h2;
                float half_h = (h / 4) / 2;
                currentYOffset = h - half_h;
            }          
        }



        if (currentEnemy != null)
        {
            if (Vector3.Distance(player.position, currentEnemy.transform.position) > detectionRange + 3f || isFixingDirection && Input.GetKeyDown(KeyCode.Tab))
            {
                LockOff();
            }
        }


        if (Input.GetKeyDown(KeyCode.Tab))
        {
            // Track the current enemy
            if (currentEnemy != null)
            {
                LockOn();
            }
        }

        /*// Fix the camera direction to the enemy when the tap key is pressed
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            isFixingDirection = !isFixingDirection;
          
            if (isFixingDirection && target != player)
            {

                // Save the current camera and player directions
                Quaternion cameraRotation = transform.rotation;
                Quaternion playerRotation = player.rotation;

                // Set the camera and player directions to face the enemy
                transform.LookAt(currentEnemy);
                //player.LookAt(currentEnemy);

                // Save the camera and player rotations for later use
                Quaternion fixedCameraRotation = transform.rotation;
                Quaternion fixedPlayerRotation = player.rotation;
              

                // Rotate the camera and player towards the fixed directions
                StartCoroutine(RotateToDirection(fixedCameraRotation, fixedPlayerRotation));
            }
            *//*else
            {
                // Restore the camera and player directions
                transform.rotation = cameraRotation;
                player.rotation = playerRotation;
            }*//*
        }*/
    }

    
    /*IEnumerator RotateToDirection(Quaternion cameraRotation, Quaternion playerRotation)
    {
        // Rotate the camera and player towards the fixed directions over time
        float elapsedTime = 0f;
        float duration = 10f;

        while (elapsedTime < duration)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, cameraRotation, elapsedTime / duration);
            player.rotation = Quaternion.Slerp(player.rotation, playerRotation, elapsedTime / duration);

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        transform.forward = 
        player.forward = transform.forward;


        // Set the camera and player directions to the fixed directions
        *//*transform.rotation = cameraRotation;
        player.rotation = playerRotation;*//*
    }*/
}