using UnityEngine;

public class BlinkEffect : MonoBehaviour
{
    public Color startColor = Color.red;
    public Color endColor = Color.white;
    [Range(0, 500)]
    public float speed = 1;

    //private Renderer ren;

    public Material mat;
    
    // Start is called before the first frame update
    void Awake()
    {
        //ren = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        mat.color = Color.Lerp(startColor, endColor, Mathf.PingPong(Time.time * speed, 1));
    }
}
